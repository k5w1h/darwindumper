//
//  main.m
//  MG
//
//  Created by Tim Debo on 5/19/14.
//
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    [ud setObject:kDD_SUFeedURL forKey:@"SUFeedURL"];
    [ud synchronize];
    return NSApplicationMain(argc, argv);
}
